# Debian Tracker presentation
A quick presentation describing the Debian Tracker bot, a python script that reads 
the number of RC bugs from UDD and posts information about them to twitter.

This will be presented in a Lightning Talk during DebConf 20.

Main Author: Francisco M Neto

email: fmneto [at] fmneto . com

irc: fmneto

**LICENSE**

Creative Commons Attribution 4.0 International License.

